# pypi

A private registry for python packages

https://gitlab.com/romk/pypi/-/packages

## Usage

To install the package `PKG-NAME` from the registry, run the command
```
pip install PKG-NAME --extra-index-url https://gitlab.com/api/v4/projects/23339513/packages/pypi/simple
```
